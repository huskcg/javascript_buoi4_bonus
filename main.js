/*Exercise 1 */
function checkValidate(day, month, year) {
  //check year
  if (year >= 1920 && year <= 275760) {
    //check month
    if (month >= 1 && month <= 12) {
      //check days
      if (
        day >= 1 &&
        day <= 31 &&
        (month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10 ||
          month == 12)
      )
        return true;
      else if (
        day >= 1 &&
        day <= 30 &&
        (month == 4 || month == 6 || month == 9 || month == 11)
      )
        return true;
      else if (day >= 1 && day <= 28 && month == 2) return true;
      else if (
        day == 29 &&
        month == 2 &&
        (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
      )
        return true;
      else {
        return alert(
          `Dữ liệu không hợp lệ:
          1. Số ngày cần >= 1 và <= 31. 
          2. Tháng 1,3,5,7,8,10,12 có 31 ngày. 
          3. Tháng 4,6,9,11 có 30 ngày. 
          4. Nếu là năm nhuận, tháng 2 chỉ có 29 ngày. 
          5. Ngược lại tháng 2 chỉ có 28 ngày`
        );
      }
    } else {
      return alert("Dữ liệu không hợp lệ, số tháng cần >= 1 và <= 12");
    }
  } else {
    return alert("Dữ liệu không hợp lệ, số năm cần >= 1920 và <= 275760");
  }
}
function calcYesterday() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  if (checkValidate(day, month, year) == true) {
    document.getElementById("result-ex1").style.display = "inline-block";
    if (day >= 2 && day <= 31) {
      day = day - 1;
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${day}/${month}/${year}`;
    } else {
      if (
        day == 1 &&
        (month == 5 || month == 7 || month == 10 || month == 12)
      ) {
        day = 30;
        month = month - 1;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      } else if (
        day == 1 &&
        (month == 2 ||
          month == 4 ||
          month == 6 ||
          month == 8 ||
          month == 9 ||
          month == 11)
      ) {
        day = 31;
        month = month - 1;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      } else if (day == 1 && month == 3) {
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
          day = 29;
          month = month - 1;
          document.getElementById(
            "result-ex1"
          ).innerHTML = `${day}/${month}/${year}`;
        } else {
          day = 28;
          month = month - 1;
          document.getElementById(
            "result-ex1"
          ).innerHTML = `${day}/${month}/${year}`;
        }
      } else {
        day = 31;
        month = 12;
        year = year - 1;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      }
    }
  } else {
    alert("Hãy kiểm tra lại dữ liệu");
    document.getElementById("result-ex1").style.display = "none";
  }
}
function calcTomorrow() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  if (checkValidate(day, month, year) == true) {
    document.getElementById("result-ex1").style.display = "inline-block";
    if (
      day >= 1 &&
      day <= 29 &&
      (month == 1 ||
        month == 3 ||
        month == 4 ||
        month == 5 ||
        month == 6 ||
        month == 7 ||
        month == 8 ||
        month == 9 ||
        month == 10 ||
        month == 11)
    ) {
      day = day + 1;
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${day}/${month}/${year}`;
    } else {
      if (
        day == 30 &&
        (month == 4 || month == 6 || month == 9 || month == 11)
      ) {
        day = 1;
        month = month + 1;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      } else if (
        day == 31 &&
        (month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10)
      ) {
        day = 1;
        month = month + 1;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      } else if (day == 31 && month == 12) {
        day = 1;
        month = 1;
        year = year + 1;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      } else {
        day = 1;
        month = 3;
        document.getElementById(
          "result-ex1"
        ).innerHTML = `${day}/${month}/${year}`;
      }
    }
  } else {
    alert("Hãy kiểm tra lại dữ liệu");
    document.getElementById("result-ex1").style.display = "none";
  }
}
/*Exercise 2 */
function calcExcercise2() {
  var month_ex2 = document.getElementById("month-ex2").value * 1;
  var year_ex2 = document.getElementById("year-ex2").value * 1;
  if (year_ex2 >= 1920 && year_ex2 <= 275760) {
    if (month_ex2 >= 1 && month_ex2 <= 12) {
      if (
        month_ex2 == 1 ||
        month_ex2 == 3 ||
        month_ex2 == 5 ||
        month_ex2 == 7 ||
        month_ex2 == 8 ||
        month_ex2 == 10 ||
        month_ex2 == 12
      ) {
        document.getElementById(
          "result-ex2"
        ).innerHTML = `Tháng ${month_ex2} năm ${year_ex2} có 31 ngày`;
      } else if (
        month_ex2 == 4 ||
        month_ex2 == 6 ||
        month_ex2 == 9 ||
        month_ex2 == 11
      ) {
        document.getElementById(
          "result-ex2"
        ).innerHTML = `Tháng ${month_ex2} năm ${year_ex2} có 30 ngày`;
      } else {
        if (year_ex2 % 400 == 0 || (year_ex2 % 4 == 0 && year_ex2 % 100 != 0)) {
          document.getElementById(
            "result-ex2"
          ).innerHTML = `Tháng ${month_ex2} năm ${year_ex2} có 29 ngày`;
        } else {
          document.getElementById(
            "result-ex2"
          ).innerHTML = `Tháng ${month_ex2} năm ${year_ex2} có 28 ngày`;
        }
      }
    } else {
      return alert("Số tháng phải >= 1 và <= 12");
    }
  } else {
    return alert("Số năm phải >= 1920 và <= 275760");
  }
}
/*Exercise 3 */
function calcExcercise3() {
  var number_ex3 = document.getElementById("number-ex3").value * 1;
  if (number_ex3 >= 100 && number_ex3 <= 999) {
    if (convert_ten() == "lẻ" && convert_unit() == "") {
      console.log(convert_hundred());
    } else if (convert_ten() == "lẻ") {
      console.log(convert_hundred(), convert_ten(), convert_unit());
    } else if (convert_unit() == "") {
      console.log(convert_hundred(), convert_ten(), convert_unit());
    } else if (convert_ten() != "mười" && convert_unit() == "một") {
      var string = "mốt";
      console.log(convert_hundred(), convert_ten(), string);
    } else {
      console.log(convert_hundred(), convert_ten(), convert_unit());
    }
  } else {
    return alert("Dữ liệu không hợp lệ. Phải là số nguyên có 3 chữ số");
  }
}
function convert_hundred() {
  var number_ex3 = document.getElementById("number-ex3").value * 1;
  var hundred = Math.floor(number_ex3 / 100);

  switch (hundred) {
    case 1:
      return "Một trăm";
    case 2:
      return "Hai trăm";
    case 3:
      return "Ba trăm";
    case 4:
      return "Bốn trăm";
    case 5:
      return "Năm trăm";
    case 6:
      return "Sáu trăm";
    case 7:
      return "Bảy trăm";
    case 8:
      return "Tám trăm";
    case 9:
      return "Chín trăm";
  }
}
function convert_ten() {
  var number_ex3 = document.getElementById("number-ex3").value * 1;
  var ten = Math.floor((number_ex3 % 100) / 10);

  switch (ten) {
    case 0:
      return "lẻ";
    case 1:
      return "mười";
    case 2:
      return "hai mươi";
    case 3:
      return "ba mươi";
    case 4:
      return "bốn mươi";
    case 5:
      return "năm mươi";
    case 6:
      return "sáu mươi";
    case 7:
      return "bảy mươi";
    case 8:
      return "tám mươi";
    case 9:
      return "chín mươi";
  }
}
function convert_unit() {
  var number_ex3 = document.getElementById("number-ex3").value * 1;
  var unit = number_ex3 % 10;
  switch (unit) {
    case 0:
      return "";
    case 1:
      return "một";
    case 2:
      return "hai";
    case 3:
      return "ba";
    case 4:
      return "bốn";
    case 5:
      return "năm";
    case 6:
      return "sáu";
    case 7:
      return "bảy";
    case 8:
      return "tám";
    case 9:
      return "chín";
  }
}
/*Exercise 4 */
function calcDistance(x1, y1, x2, y2) {
  if (x1 >= 0 && y1 >= 0 && x2 >= 0 && y2 >= 0) {
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
  } else {
    return alert("Toạ độ là số nguyên dương");
  }
}
function calcExcercise4() {
  var student1 = document.getElementById("name-student1").value;
  var x_student1 = document.getElementById("x-student1").value * 1;
  var y_student1 = document.getElementById("y-student1").value * 1;

  var student2 = document.getElementById("name-student2").value;
  var x_student2 = document.getElementById("x-student2").value * 1;
  var y_student2 = document.getElementById("y-student2").value * 1;

  var student3 = document.getElementById("name-student3").value;
  var x_student3 = document.getElementById("x-student3").value * 1;
  var y_student3 = document.getElementById("y-student3").value * 1;

  var x_school = document.getElementById("x-school").value * 1;
  var y_school = document.getElementById("y-school").value * 1;

  var d1 = calcDistance(x_student1, y_student1, x_school, y_school);
  var d2 = calcDistance(x_student2, y_student2, x_school, y_school);
  var d3 = calcDistance(x_student3, y_student3, x_school, y_school);
  console.log(d1, d2, d3);
  if (d1 > d2 && d1 > d3) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student1}`;
  } else if (d2 > d1 && d2 > d3) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student2}`;
  } else if (d3 > d1 && d3 > d2) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student3}`;
  } else if (d1 == d2 && d1 > d3) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student1} và ${student2} bằng nhau và xa trường nhất`;
  } else if (d1 == d3 && d1 > d2) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student1} và ${student3} bằng nhau và xa trường nhất`;
  } else if (d2 == d3 && d2 > d1) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student2} và ${student3} bằng nhau và xa trường nhất`;
  } else if (d1 == d2 && d2 == d3) {
    document.getElementById(
      "result-ex4"
    ).innerHTML = `Sinh viên xa trường nhất: ${student1}, ${student2} và ${student3} bằng nhau và xa trường nhất`;
  }
}
